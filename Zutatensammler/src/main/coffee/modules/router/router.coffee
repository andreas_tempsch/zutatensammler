define [
  "modules/login/LoginController",
  "modules/home/HomeController",
  "text!templates/home.html",
  "text!templates/login.html"
], (loginController, homeController, homeTemplate, loginTemplate) ->
  routes =
    home:
      route: '/home'
      controller: homeController
      template: homeTemplate
    login:
      route: '/'
      controller: loginController
      template: loginTemplate
