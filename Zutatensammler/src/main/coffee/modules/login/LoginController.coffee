define [], () ->
  ($scope,$resource,$location,auth) ->
    $scope.word = /^\w*$/
    $scope.logIn = ()->
      auth.logIn(
        user: $scope.credentials.user
        pass: $scope.credentials.pass
        resource: $resource
        location: $location
        success: ->
          $location.path('/home') if auth.loggedIn()
      )
    return

