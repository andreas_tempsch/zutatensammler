((root) ->
  require ["config"], (config) ->
    $.extend config.shim,
      'jasmine':
        deps: ['jquery','ember']
        exports: 'jasmine'
      'jasmine-html':
        deps: ['jasmine']
        exports: 'jasmine'
    $.extend config.paths,
      'jasmine': 'libs/jasmine'
      'jasmine-html': 'libs/jasmine-html'
      'spec': 'app/tests'
    requirejs.config config
    require(['jquery', 'jasmine-html'], ($, jasmine) ->

      jasmineEnv = jasmine.getEnv()
      jasmineEnv.updateInterval = 1000

      htmlReporter = new jasmine.HtmlReporter();

      jasmineEnv.addReporter(htmlReporter);

      jasmineEnv.specFilter = (spec) ->
        htmlReporter.specFilter spec

      specs = [];
      specs.push 'spec/testSpec'


      $(->
        require specs, ->
          jasmineEnv.execute()
        )
    )
)(this)