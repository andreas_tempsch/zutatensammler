((root) ->
  require ["config"], (config) ->
    requirejs.config config
    require ["app", "lm"], (App, LM) ->
      App.initialize();
      LM.init();
      $('body').observe 'childlist subtree', (record) ->
        LM.invokeChildrenAdjustment record.target
)(this)