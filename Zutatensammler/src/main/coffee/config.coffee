define
  app_name: "Zutaten"
  shim:
    'angular':
      exports: 'angular'
    'Underscore':
      exports: '_'
    ngResource:
      exports: 'angular'
      deps: ['angular']
    'lm':
      deps: ['observe', 'dimensions']
      exports: 'LayoutManager'
  paths:
    'angular': 'libs/angular'
    'Underscore': 'libs/underscore'
    'ngResource': 'libs/angular-resource'
    'text': 'libs/require-text'
    'i18n': 'libs/i18n'
    'lm': 'libs/LayoutManager'
    'observe': 'libs/jquery.observe'
    'dimensions': 'libs/jquery.dimensions'
    'router': 'modules/router'
  config:
    i18n:
      locale: (->
        "root"
      )()