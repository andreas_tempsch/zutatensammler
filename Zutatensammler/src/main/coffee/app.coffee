define [
  "angular",
  "router/router",
  "Underscore",
  "services/authentication/AuthenticateService",
  "ngResource"
], (angular, router, _, AuthenticateService) ->
  initialize: ->
    app = angular.module "zutaten", ['ngResource'], ($routeProvider, $locationProvider) ->

      _.each router, (value, key) ->
        $routeProvider.when value.route,
          template: value.template
          controller: value.controller

      $routeProvider.otherwise
        redirectTo: '/'

      $locationProvider.html5Mode true

    app.factory "auth", AuthenticateService

    angular.bootstrap document, ['zutaten']

    return