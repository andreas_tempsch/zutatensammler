define [], ->
  _authenticated = false
  _user = ''
  _rights = {}
  ->
    loggedIn: ->
      _authenticated
    getUser: ->
      _user
    logIn: (toLogin)->
      con = toLogin.resource '/logMeIn'
      details = con.get {user: toLogin.user, pass: toLogin.pass}, (->
        _authenticated = true if details.user
        _user = details.user if _authenticated
        toLogin.success()
      ), (->
        console.log "Login failed"
      )
