var mongodb = require('mongodb');
var express = require('express');
var http = require('http');
var path = require('path');
var BSON = mongodb.BSONPure;

var Db = mongodb.Db;
var Server = mongodb.Server;

var app = express();
app.use(express.static(__dirname + "/public"));


var client = new Db('expenseApp', new Server("127.0.0.1", 27017, {}));
app.use(express.bodyParser());

app.get('/available_expenses', function (req, res) {
    client.collection('availableExpenses', function (err, collection) {
        collection.find().toArray(function (err, results) {
            var toSend = {"available_expenses": results};
            res.json(200, toSend);
        });
    });
});
app.get('/expensesList', function (req, res) {
    client.collection('expensesList', function (err, collection) {
        collection.find().toArray(function (err, results) {
            res.json(200, results);
        });
    });
});
app.post('/insertIntoExpensesList', function (req, res) {
    var expense = req.body;
    client.collection('expensesList', function (err, collection) {
        collection.insert(expense, {safe: true}, function (err, results) {
            if (err) {
                res.send({'error': err});
            } else {
                res.send(expense);
            }
        });
    });
});
app.post('/deleteFromExpensesList', function (req, res) {
    var id = req.body._id;
    client.collection('expensesList', function (err, collection) {
        collection.remove({'_id': new BSON.ObjectID(id)}, {safe: true}, function (err, results) {
            console.log('Success: ' + id);
            res.send(id);
        });
    });
});
app.get('/logMeIn', function (req, res) {
    var user = req.query.user;
    var pass = req.query.pass;
    if (user && pass) {
        client.collection('userList', function (err, collection) {
            collection.findOne({'user': user}, function (err, item) {
                if (item && item.pass == pass) {
                    res.send({user: user, authID: "1234fancyAuthID", group: item.group});
                } else {
                    res.send({});
                }
            });
        });
    } else {
        res.send({});
    }

});

client.open(function (err, db) {
    if (!err) {
        console.log("Connected to 'expenseApp' database");
        db.collection('availableExpenses', {strict: true}, function (err, collection) {
            if (err) {
                console.log("The 'availableExpenses' collection doesn't exist. Creating it with sample data...");
                populateDB();
            }
        });
        db.collection('expensesList', {strict: true}, function (err, collection) {
            if (err) {
                console.log("The 'expensesList' collection doesn't exist. Creating it with sample data...");
                populateDB2();
            }
        });
        db.collection('userList', {strict: true}, function (err, collection) {
            if (err) {
                console.log("The 'userList' collection doesn't exist. Creating it with sample data...");
                populateDB3();
            }
        });
    }
});

var populateDB = function () {

    var expenses = [
        {
            title: "Tanken",
            type: "expense"
        },
        {
            title: "Essen",
            type: "expense"
        },
        {
            title: "Arbeit",
            type: "income"
        },
        {
            title: "Geschenk",
            type: "income"
        }
    ];

    client.collection('availableExpenses', function (err, collection) {
        collection.insert(expenses, {safe: true}, function (err, result) {
        });
    });

};

var populateDB2 = function () {

    var expensesList = [
        {
            title: "Tanken",
            type: "expense",
            amount: 80
        },
        {
            title: "Essen",
            type: "expense",
            amount: 40
        },
        {
            title: "Arbeit",
            type: "income",
            amount: 2500
        },
        {
            title: "Geschenk",
            type: "income",
            amount: 100
        }
    ];

    client.collection('expensesList', function (err, collection) {
        collection.insert(expensesList, {safe: true}, function (err, result) {
        });
    });

};

var populateDB3 = function () {

    var userList = [
        {
            user: "asdf",
            pass: "asdf",
            group: "admin"
        },
        {
            user: "qwer",
            pass: "qwer",
            group: "user"
        }
    ];

    client.collection('userList', function (err, collection) {
        collection.insert(userList, {safe: true}, function (err, result) {
        });
    });

};

app.listen(3000);